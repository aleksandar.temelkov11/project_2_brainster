import { useRoutes } from 'hookrouter';
import { Provider } from './context/Context';
import routes from './router/routes';
import './App.css';
import './styles/Btns.css'
import './styles/Universal.css'

function App() {
  const router = useRoutes(routes)
  return (
    <Provider>
      {router}
    </Provider>
  );
}

export default App;

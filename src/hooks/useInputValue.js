import { useState } from 'react'; 
export default function useInputValue(init) {
    const [inputValue, setInputValue] = useState(init);

    const onValueChange = (e) => {
        const { name, value } = e.target;
        setInputValue({
          ...inputValue,
          [name]: value,
        });
      };

    return [inputValue, onValueChange]
}

 
import { useEffect, useState } from "react";

const useArrayValue = (init) => {
    const [array, setArray] = useState(init);


    const onArrayChange = (value) => {
        setArray(array => [...array, value])
    }
    return [array, onArrayChange]
}

export default useArrayValue;
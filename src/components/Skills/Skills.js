import React from 'react';

const Skills = ({data,badge}) => {
    return (
        <div className="Skills">
            <h2>Skills & Compentencies</h2>
            {data.map((el,idx) => {
                return(
                    <span 
                    key={idx}
                    className={badge}
                    >{el}</span>
                )
            })}
        </div>
    );
};

export default Skills;
import React from 'react';
import Remove from '../AddRemove/Remove';
import Input from '../SingleElements/Input';

const InputHandler2 = ({name,cls,ph}) => {
    return (
        <div className="InputHandler">
            <Remove />
            <Input name={name} cls={cls} ph={ph} />
        </div>
    );
};

export default InputHandler2;
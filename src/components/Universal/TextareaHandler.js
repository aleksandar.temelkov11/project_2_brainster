import React from 'react';
import Add from '../AddRemove/Add';
import Remove from '../AddRemove/Remove';
import Textarea from '../SingleElements/Textarea';

const TextareaHandler = ({cls, ph, value}) => {
    return (
        <div className="TextareaHandler">
            <Add value={value} />
            <Remove />
            <Textarea cls={cls} ph={ph} />

        </div>
    );
};

export default TextareaHandler;
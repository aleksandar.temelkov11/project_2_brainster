import React from 'react';
import Add from '../AddRemove/Add';
import Remove from '../AddRemove/Remove';
import Input from '../SingleElements/Input';

const InputHandler = ({name,cls,ph,value}) => {
    return (
        <div className="InputHandler">
            <Add value={value} />
            <Remove />
            <Input name={name} cls={cls} ph={ph} />
        </div>
    );
};

export default InputHandler;
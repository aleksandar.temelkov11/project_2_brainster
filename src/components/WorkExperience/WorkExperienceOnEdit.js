import React from 'react';
import { workExperience } from '../../data/DummyData';
import useArrayValue from '../../hooks/useArrayValue';
import Textarea from '../SingleElements/Textarea';
import InputHandler from '../Universal/InputHandler';
import TextareaHandler from '../Universal/TextareaHandler';

const WorkExperienceOnEdit = (props) => {
    const [array,onArrayChange] = useArrayValue(workExperience);

    return (
        <div className="WorkExperience">
            <InputHandler 
            name="workExperience"
            cls="headers"
            ph="Work Experience"
            value={workExperience}
            />
            {array.map((el,idx) => {
                return (
                    <div key={idx} className="exp">

                    <InputHandler
                            name="position"
                            cls="position"
                            ph={el.position}
                             />

                        <InputHandler
                            name="company"
                            cls="company"
                            ph={el.company}
                            
                        />

                        <div className="date-city">
                            <InputHandler
                                name="date"
                                cls="date"
                                ph={el.date}
                                
                            />

                            <InputHandler
                                name="city"
                                cls="city"
                                ph={el.city}
                                
                            />
                            
                        </div>
                        <span className="tasks">Acomplishments/Tasks</span>
                        
                        {el.accomplishments.map((el,idx) => 
                        (<ul key={idx}>
                            <li>
                                <TextareaHandler
                                cls="accomplishment"
                                ph={el}
                                />
                            </li>
                        </ul>)
                        )}
                        
                    </div>
                )
            })}
        </div>
    );
};

export default WorkExperienceOnEdit;
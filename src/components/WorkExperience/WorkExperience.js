import React from 'react';

const WorkExperience = ({data,category}) => {
    return (
        <div className={`${category}WorkExperience WorkExperience`}>
            <h2>Work Experience</h2>
            <ul>
            {data.map((el,idx) => {
                return(
                    <div key={idx} className={`${category}-exp exp`}>
                        <li>
                            <h3 className="position">{el.position}</h3>
                            <h3 className="company">{el.company}</h3>
                            <div className={`${category}-date-city date-city`}>
                                <span className="date">{el.date}</span>
                                <span className="city">{el.city}</span>
                            </div>
                            <div className={`${category}-acc acc`}>
                                <span className="tasks">Accomplishments/Tasks</span>
                                <ul >
                                {el.accomplishments.map((el,idx) => (
                                        <li key={idx}>
                                            <span className={`acc${idx}`}>
                                                {el}
                                            </span>
                                        </li>
                                ))}
                                </ul>
                            </div>
                        </li>
                </div>
                )
                
            })}
            </ul>
        </div>
    );
};

export default WorkExperience;

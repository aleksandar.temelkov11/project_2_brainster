import React from 'react';

const Interests = ({ data }) => {
    return (
        <div className="Interests">
            <h2 className="heading">Interests</h2>
            {data.map((el, idx) => {
                return (
                    <div key={idx} className="interest"> 
                        {<i className={el.icon}></i>}
                        <span >{el.name}</span>
                    </div>
                )

            })}
        </div>
    );
};

export default Interests;
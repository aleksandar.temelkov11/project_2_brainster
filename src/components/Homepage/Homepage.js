import React from 'react';
import HomepageHeading from './HomepageHeading';
import Image from '../SingleElements/Image';
import HomepageFooter from './HomepageFooter';
import '../../styles/Homepage.css';

const Homepage = (props) => {

    return (
        <div className="Homepage">
            <HomepageHeading />
            <Image img="images/backgrounds/BackgroundFirstPage.png" />
            <HomepageFooter />
            
            
        </div>
    );
};

export default Homepage;
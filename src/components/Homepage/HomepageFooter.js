import React from 'react';

const HomepageFooter = () => {
    const love = "<3";
    return (
        <div className="HomepageFooter">
            <h4>Made with {love} by the <a href="https://brainster.co/"
                target="_blank" rel="noreferrer">
                Brainster Coding Academy</a> Students and
                <a href="https://www.wearelaika.com/" 
                target="_blank" 
                rel="noreferrer"> Wearelaika </a>
            </h4>
        </div>
    );
};

export default HomepageFooter;
import React from 'react';
import { A } from 'hookrouter';

const HomepageHeading = () => {
    return (
        <div className="HomepageHeading">
            <h1>The Ultimate <span>CV & Portfolio Check-List</span> </h1>
            <h4>Are you a Web Developer, Data Scientist, Digital Marketer or a Designer? Have your CV and portfolio in check and create a 5 star representation of your skills with this guide.</h4>
            <A href='/cv-builder/Categories'><button className="btn btn-purple">STEP INSIDE</button></A>
        </div>
    );
};

export default HomepageHeading;
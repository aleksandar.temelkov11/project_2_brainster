import React from 'react';
import useArrayValue from '../../hooks/useArrayValue';

const Add = ({value}) => {
    const [array, onArrayChange] = useArrayValue([])

    return (
        <div className="Add">
            <button>+</button>
        </div>
        
    );
};

export default Add;
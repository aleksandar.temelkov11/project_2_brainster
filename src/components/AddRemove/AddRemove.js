import React from 'react';
import Add from './Add';
import Remove from './Remove';

const AddRemove = ({value}) => {
    return (
        <div className="AddRemove">
            <Add value={value} />
            <Remove />
        </div>
    );
};

export default AddRemove;
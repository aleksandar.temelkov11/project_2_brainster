import React, { useContext } from 'react';
import {Context} from '../../context/Context';

const Remove = (props) => {
    const { onRemove } = useContext(Context);

    return (
        <div className="Remove">
        <button onClick={onRemove}>-</button>
        </div>
        
    );
};

export default Remove;
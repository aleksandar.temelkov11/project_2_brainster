import React from 'react';
import { A } from 'hookrouter';
import routingData from '../../data/routingData';
import '../../styles/Categories.css';

const Categories = () => {
    return (
        <div className="Categories">
            <h1>Choose your category</h1>
            <div className="wrapper">
            {routingData.map((el,idx) => {
                return (
                    <div key={idx} className="box">
                        <h2>{el.title}</h2>
                        <A href={`/cv-builder${el.route}`}>
                        <button className="btn btn-purple">Choose</button>
                        </A>
                    </div>
                )
            })}
            </div>
        </div>
    );
};

export default Categories;
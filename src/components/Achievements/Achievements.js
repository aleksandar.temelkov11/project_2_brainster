import React from 'react';

const Achievements = ({data,category}) => {
    return (
        <div className="Achievements">
            <h2>Achievements & Certificates</h2>
            {data.map((el,idx) => {
                return(
                    <div key={idx}>
                        <p className={`${category}achievement achievement`}>{el.cer}</p>
                        {el.detail && 
                        <span className={`${category}detail detail`}>{el.detail}</span>}
                    </div>
                )
            })}
        </div>
    );
};

export default Achievements;
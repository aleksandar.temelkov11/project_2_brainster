import React, { useContext } from 'react';
import { Context } from '../../context/Context';
import { A } from 'hookrouter';

const Buttons = ({ category }) => {
    const { 
        btns,
        toggleActive,
        toggleActiveStyles,
        activateEditMode,
    } = useContext(Context);


    return (
        <div className="Buttons">
            {btns.map((el, idx) => {
                return (
                    <A key={idx} href={`/cv-builder${category}/${el}`} >
                        <button
                            className={toggleActiveStyles(idx)}
                            onClick={() => toggleActive(idx)}>
                            {el}
                        </button>
                    </A>
                )
            })}
            <button
                className="btn btn-purple2"
                onClick={activateEditMode}>
                Edit
            </button>
        </div>
    );
};

export default Buttons;
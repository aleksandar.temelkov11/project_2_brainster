import React from 'react';
import Footer from '../SingleElements/Footer';
import '../../styles/Footer.css'

const GlobalFooter = () => {
    return (
        <div className="GlobalFooter">
            <Footer cls="brainster" txt="Do you want to learn hand-on digital skills?" wbs="https://brainster.co/" btn="VISIT BRAINSTER" />
            
            <Footer cls="laika" txt="Do you want to receive job offers by IT companies?" wbs="https://www.wearelaika.com/" btn="VISIT LAIKA" />
            
        </div>
    );
};

export default GlobalFooter;
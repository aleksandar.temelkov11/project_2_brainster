import React from 'react';

const Info = ({data}) => {
    return (
        <div className="Info">
            <div><p>{data.email}</p> </div>
            <div><p>{data.phone}</p></div>
            <div><p>{data.location}</p></div>
            <div><p>{data.twitter}</p></div>
        </div>
    );
};

export default Info;
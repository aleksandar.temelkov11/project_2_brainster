import React from 'react';

const About = ({data}) => {
    return (
        <div className="About">
            <h1 className="fullName">{data.fullName}</h1>
            <h3 className="profTitle">{data.profTitle}</h3>
            <p className="aboutMe">{data.aboutMe}</p>
        </div>
    );
};

export default About;
import React from 'react';
import InputHandler2 from '../Universal/InputHandler2';

const InfoOnEdit = () => {
    return (
        <div className="Info">
            <InputHandler2 
            name="email"
            cls="p-info"
            ph="E-mail"
            />
            <InputHandler2 
            name="phone"
            cls="p-info"
            ph="Phone Number"
            />
            <InputHandler2 
            name="location"
            cls="p-info"
            ph="City, Country"
            />
            <InputHandler2 
            name="twitter"
            cls="p-info"
            ph="Twitter"
            />
        </div>
    );
};

export default InfoOnEdit;
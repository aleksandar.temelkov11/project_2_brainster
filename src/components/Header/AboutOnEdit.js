import React from 'react';
import Input from '../SingleElements/Input';
import Textarea from '../SingleElements/Textarea';
import Remove from '../AddRemove/Remove';

const AboutOnEdit = () => {
    return (
        <div className="About">
            <Input 
            name="fullName"
            cls="fullName"
            ph="Full Name"
            />
            <Textarea 
            name="profTitle"
            cls="profTitle"
            ph="Proffesional Title"
            />
            <div className="InputHandler">
            <Remove />
            <Textarea 
            name="aboutMe"
            cls="aboutMe"
            ph="About Me"
            />
            </div>
            
        </div>
    );
};

export default AboutOnEdit;
import React from 'react';

const Textarea = ({cls,ph}) => {
    return (
        <span
        className={`textarea ${cls}`}
        data-placeholder={ph}
        role="textbox"
        contentEditable
        ></span>
    );
};

export default Textarea;
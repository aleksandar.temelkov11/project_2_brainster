import React from 'react';

const UploadImage = (props) => {
    return (
        <div className="Image">
            <img src="../../images/CV-images/addimg.png" alt="img" />
        </div>
    );
};

export default UploadImage;
import React, { useContext } from 'react';
import { Context } from '../../context/Context';
import InputData from '../../data/InputData';

import useInputValue from '../../hooks/useInputValue';

const Input = ({name,cls,ph}) => {
    const [inputValue, onValueChange] = useInputValue(InputData);
    const {onActiveChange} = useContext(Context);
    return (
            <input 
            name={name}
            className={cls}
            placeholder={ph}
            value={inputValue.name}
            onChange={onValueChange}
            onClick={onActiveChange}
            />
            
        
    );
};

export default Input;
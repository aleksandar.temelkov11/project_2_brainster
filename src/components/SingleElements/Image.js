import React from 'react';

const Image = ({img}) => {
    return (
        <div className="Image">
            <img src={img} alt="img" />
        </div>
    );
};

export default Image;
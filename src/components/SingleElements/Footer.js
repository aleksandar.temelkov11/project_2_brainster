import React from 'react';

const Footer = ({ cls, txt, wbs, btn }) => {
    return (
        <div className={cls}>
            <p>{txt}</p>
            <a href={wbs} rel="noreferrer" target="_blank"><button className="btn btn-black">{btn}</button></a>
        </div>
    );
};

export default Footer;
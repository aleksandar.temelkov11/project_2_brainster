import React, { useContext,  } from 'react';
import { Context } from '../../context/Context';
import Buttons from '../Buttons/Buttons';
import TipsAndTricks from '../Universal/TipsAndTricks';
import WebInformationBox from './WebInformationBox';
import '../../styles/WebDevelopment.css'


const WebDevelopment = (props) => {
    const data = "";
    const { editMode, activeBtn, tips } = useContext(Context);

    
    const CV = activeBtn === 'CV';
    const L = activeBtn === 'LinkedIn';

    return (
        <div className="WebDevelopment">
            <div className="ContentWrapper">
            <Buttons category="/Web-Development" />
            {CV && <WebInformationBox  editMode={editMode} />}
            {/* {CV && <WebInformationBox editMode={editMode} />}
            {L && <LinkedIn editMode={editMode} />} */}
            </div>
            
            <TipsAndTricks />
        </div>
    );
};

export default WebDevelopment;

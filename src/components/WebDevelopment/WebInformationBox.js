import React from 'react';
import ElonData from '../../data/ElonData';
import About from '../Header/About';
import Image from '../SingleElements/Image';
import Info from '../Header/Info';
import WorkExperience from '../WorkExperience/WorkExperience';
import Skills from '../Skills/Skills';
import Achievements from '../Achievements/Achievements';
import Interests from '../Interests/Interests';
import AboutOnEdit from '../Header/AboutOnEdit';
import UploadImage from '../SingleElements/UploadImage';
import InfoOnEdit from '../Header/InfoOnEdit';
import WorkExperienceOnEdit from '../WorkExperience/WorkExperienceOnEdit';
import EducationOnEdit from '../Education/EducationOnEdit';

const WebInformationBox = ({editMode}) => {
    return (
        <div className="InformationBox">
            <div className="WebHeader Header">
                {editMode ? <AboutOnEdit /> : <About data={ElonData.about} />}
                {editMode ? <UploadImage /> : <Image img="../../images/CV-images/elon.jpg" />}
                {editMode ? <InfoOnEdit /> : <Info data={ElonData.info} />}
            </div>
            <div className="WorkEducation">
                {editMode ? <WorkExperienceOnEdit/> : <WorkExperience data={ElonData.workExperience} category="Web" />}
                {editMode && <EducationOnEdit />}
            </div>
            <div className="SkillsAchievements">
                <Skills data={ElonData.skills} badge="badge badge-info" />
                <Achievements data={ElonData.achievements} />
                <Interests data={ElonData.interests} />
            </div>
        </div>
    );
};

export default WebInformationBox;
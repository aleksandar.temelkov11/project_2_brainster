import React from 'react';
import Homepage from '../components/Homepage/Homepage';
import Categories from '../components/Categories/Categories';
import WebDevelopment from '../components/WebDevelopment/WebDevelopment';



const routes = {
    '/cv-builder' : () => (
        <Homepage />
    ),
    '/cv-builder/Categories' : () => (
        <Categories />
    ),
    '/cv-builder/Web-Development' : () => (
        <WebDevelopment />
    ),
    '/cv-builder/Web-Development/:type'  : (type) => (
        <WebDevelopment type={type} />
    ),

    // '/cv-builder/Data-Science' : () => (
    //     <DataScience />
    // ),
    // '/cv-builder/Data-Science/:type' : (type) => (
    //     <DataScience type={type} />
    // ),
    // '/cv-builder/Digital-Marketing' : () => (
    //     <DigitalMarketing />
    // ),
    // '/cv-builder/Digital-Marketing/:type' : (type) => (
    //     <DigitalMarketing type={type}/>
    // ),
    // '/cv-builder/Design' : () => (
    //     <Design />
    // ),
    // '/cv-builder/Design/:type' : (type) => (
    //     <Design  type={type} />
    // ),
    
    
    
}

export default routes;
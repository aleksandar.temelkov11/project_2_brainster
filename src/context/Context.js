import { useRedirect } from "hookrouter";
import { createContext, useEffect, useState } from "react";
import GlobalFooter from '../components/Footer/GlobalFooter';

export const Context = createContext({});

export const Provider = (props) => {
    useRedirect('/', '/cv-builder');
    const currentPathName = window.location.pathname;
    const type = currentPathName.split('/').slice(3, 4).toString();
    const firstLast = '/cv-builder' || '/cv-builder/pop-up';

    const [btnState, setBtnState] = useState({
        activeState: null,
        objects: [
            'CV', 'LinkedIn', 'WEARELAIKA'
        ],
    })
    const btns = btnState.objects;
    const activeBtn = btnState.activeState;
    


    useEffect(() => {
        setBtnState({
            ...btnState,
            activeState: btns.find(el => el === type)
        })
    }, [])



    const toggleActive = (idx) => {
        setBtnState({
            ...btnState,
            activeState: btns[idx]
        })
    }

    const toggleActiveStyles = (idx) => {
        if (btns[idx] === btnState.activeState) {
            return 'btn active'
        }
        else {
            return 'btn btn-white'
        }
    }

    const [editMode, setEditMode] = useState(false);

    const activateEditMode = () => {
        setEditMode(true);
        // const allInputs = document.querySelectorAll('input');
        // allInputs.forEach((input) => {
        //     input.removeAttribute('readOnly')
        // })

        const allTextAreas = document.querySelectorAll('.textarea');
        allTextAreas.forEach(ta => {
            ta.setAttribute("contentEditable", true);
        })

        const allBadges = document.querySelectorAll('.badge');
        allBadges.forEach(badge => {
            badge.setAttribute("contentEditable", true);
        })
    }

    // const [tips, setTips] = useState('Here be lie the tips');

    // const activateTipsOnClick = (e,value) => {
    //     let className = e.target.className;
        
    //     // if(Object.keys(tipsAndTricks).find(el => el === className) ) {
    //     //     let assignedTips = Object.keys(tipsAndTricks).map(k => tipsAndTricks[k])
    //     //     setTips(assignedTips)
    //     // }
    //     Object.keys(value).find(el => {
    //         if (className.includes('textarea')) {
    //             var newClassName = className.slice(9, Infinity)
    //             if(el === newClassName) {
    //                 setTips(value[el])
    //             }
    //         }

    //         if(el === className){
                
    //             setTips(value[el])
    //         }
    //     })
        

    //     // console.log(className)
        

    //     // console.log(Object.keys(tipsAndTricks))

    //     // console.log(objectKeys(tipsAndTricks[i]))

    //     // console.log(className)
        



    // }

    // const addElement = (e) => {
    //     // console.log(e.target.nextSibling.className);
    //     console.log(e.target.parentElement)
        
    // }

    const onActiveChange = (e) => {
        // const button = e.target.parentElement.firstChild;
        
        //  let removeButtons = document.querySelectorAll(".Remove");
        //  let addButtons = document.querySelectorAll(".Add");
        //  removeButtons.forEach(el => {
        //      el.setAttribute('style','display:none;')
        //  })
        //  addButtons.forEach(el => {
        //     el.setAttribute('style','display:none;')
        // })
         
        //  button.setAttribute('style','display:initial;');
        //  console.log

        // const addActive = e.target.parentElement.parentElement.querySelector('.Add');
        // const removeActive = e.target.parentElement.parentElement.querySelector('.Remove') ;
        // let removeButtons = document.querySelectorAll(".Remove");
        // let addButtons = document.querySelectorAll(".Add");

        // addButtons.forEach(el => {
        //     el.setAttribute('style','display:none;')
        // });

        // removeButtons.forEach(el => {
        //          el.setAttribute('style','display:none;')
        //      });

            
        //      addActive.setAttribute('style','display:initial;');
        //      removeActive.setAttribute('style','display:initial;');
         
     }

    const onRemove = (e) => {
        e.target.parentElement.parentElement.remove()
    }




    const contextObject = {
        btns,
        activeBtn,
        toggleActive,
        toggleActiveStyles,
        editMode,
        activateEditMode,
        onActiveChange,
        onRemove,
        // tips,
        // activateTipsOnClick,
        // addElement,

    };

    return (
        <Context.Provider value={contextObject}>
            {props.children}
            {currentPathName !== firstLast && <GlobalFooter />  }
        </Context.Provider>
    )
}
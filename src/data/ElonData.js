const ElonData = {
        about: {
            fullName:"Elon Musk",
            profTitle:"Entrepreneur, Engineer, Inventor and Investor",
            aboutMe:"Aiming to reduce global warming through sustainable energy production and consumption. Planning to reduce the risk of human extinction by making life multi-planetary and setting up a human colony on Mars",
        },
            
        
        info: {
                email: "elon@teslamotors.com",
                phone: "620-681-5000",
                location: "Los Angeles, USA",
                twitter: "@elonmusk",
        },
    
    workExperience: [{
        position: "Founder, CEO & Lead Designer",
        company: "SpaceX Space Exploration Technologies",
        date: "06/2002 - Present",
        city: "Howthorne, USA",
        accomplishments: [
            "Successfully lauched Falcon Heavy, the most powerful operation rocked in the world by a factor of two, with the ability to lift into orbit nearly 64 metric ton (141,000 lb) --a mass greater than a 737 jetliner loaded with passengers, crew, luggage and fuel.",
            "Plans to reduce space transportation cost to enable people to colonize Mars.",
            "Developed the Falcon 0  spacecraft which replaced the space shuttle when in retired in 2011.",
        ],

    },
    {
        position: "Founder",
        company: "The Boring Company",
        date: "12/2016 - Present",
        city: "Howthorne, USA",
        accomplishments: [
            "Raised $10m by selling 20.000 flamethrowers in 4 days.",
            "Raised $1m by selling 50/00 baseball caps.",
            "Hyperloop -- an ultra high-speed underground public transportation system in which passengers are transported on autonomous elctric pods traveling at 600+ miles per hour in a pressurized cabin.",],

    },
    {
        position: "CEO and Product Architect",
        company: "Tesla Inc.",
        date: "06/2014 - Present",
        city: "San Mateo, USA",
        accomplishments: [
            "Global sales passed 250,000 units in September 2017.",
            "Consumer Reports named Tesla as the top American car brand and ranked it 8th among the global carmakes in February 2017.",
            "Topped Consummer Reports Annual Owner Satisfation Survey at 91% in 2016.",],

    },
    {
        position: "Co-founder and Former Chairman",
        company: "SolarCity (subsidiary of Tesla Inc.)",
        date: "06/2006 - Present",
        city: "San Mateo, USA",
        accomplishments: [
            "Merged the company with Tesla Inc. and now offers energy storage services throught Tesla, including a turnkey residential battery backup service that incorporates Tesla's Powerwall.",
            "In 2015, installed 870MW of solar power, approximately 28% of non utility solar installation in the U.S. that year.",],
    },
    {
        position: "Founder & CEO",
        company: "Neurolink",
        date: "07/2016 - Present",
        city: "San Francisco, USA",
        accomplishments: ["A company aims to make devices to treat serious brain diseases in short-term, with the eventual goal of human enchacement.",],

    },
    ],

    

        skills: ["Thinking through first principles", "Marketing", "Micromanagement", "Goal oriented", "Resiliency", "Future focused", "Leadership", "Creativity", "Time Management", "Persistence", "Turning ideas into companies", "Long-term thinking"],

        achievements: [
            { cer: "Actually Richest person in the world - 2021" },
            { cer: "21st on the Forbes list of the Worlds's Most Powerful People (2016)" },
            {
                cer: "IEEE Honorary Membership (2015)",
                detail: "Given to people who have rendered meritoriuos service to humanity in the IEEE's designated fields of intrest."
            },
            { cer: "Bisinessperson of the Year by Fortune Magazine(2013)" },
            {
                cer: "FAI Gold Space Medal (2010)",
                detail: "One of the highest honours in the aerospace industry shared with prominent personalities like Neil Armsstrong and John Glenn."
            },
            { cer: "Honorary Doctorate in Design from the Art Center College of Design" },
            { cer: "Honorary Doctorate (DUniv) in Aerospace Engeneering from the University of Surrey" },
            { cer: "Honorary Doctorate of Engeneering and Technology from Yale University" }
        ],

        interests: [
            {
                name: "Psysics",
                icon: "fab fa-grav fa-lg"

            },
            {
                name: "Alternative Energy",
                icon: "fas fa-sun fa-lg"
            },
            {
                name: "Sustainability",
                icon: "fas fa-leaf fa-lg"
            },
            {
                name: "Space Engineering",
                icon: "fas fa-rocket fa-lg"
            },
            {
                name: "Philanthropy",
                icon: "fas fa-hand-rock fa-lg"
            },
            {
                name: "Reading",
                icon: "fas fa-book fa-lg"
            },
            {
                name: "Twitter",
                icon: "fab fa-twitter fa-lg"
            },
            {
                name: "Video Games",
                icon: "fas fa-gamepad fa-lg"
            },
            {
                name: "Extraterrestrial Life",
                icon: "fab fa-reddit-alien fa-lg"
            },
            {
                name: "AI",
                icon: "fas fa-microchip fa-lg"
            },]
    }





export default ElonData;
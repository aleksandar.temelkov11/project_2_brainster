const routingData = [
    {
        title: "Web Development",
        route: "/Web-Development",
        
    },
    {
        title: "Data Science",
        route: "/Data-Science",
        
    },
    {
        title: "Digital Marketing",
        route: "/Digital-Marketing",
        
    },
    {
        title: "Design",
        route: "/Design",
        
    },
    
]

export default routingData;


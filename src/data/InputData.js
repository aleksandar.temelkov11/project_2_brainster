const InputData = {
    fullName: "",
    profTitle: "",
    email: "",
    phone: "",
    location: "",
    twitter: "",
    linkedIn: "",
    position: "",
    company: "",
    date: "",
    city: "",
}

export default InputData;